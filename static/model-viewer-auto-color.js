function getModelViewer()
	{ return document.currentScript.parentNode }

function setModelColor(modelViewer, rgb)
	{ modelViewer.model.materials[0].pbrMetallicRoughness.setBaseColorFactor(rgb.slice(0,3).map(e => e/255)) }

function getStyleProp(element, property)
	{ return window.getComputedStyle(element).getPropertyValue(property)}

function getColor(element)
	{ return Array.from(getStyleProp(element, 'color').matchAll('[0-9]+'), e => parseInt(e[0])) }

getModelViewer().addEventListener('load', e => {
	setModelColor(e.path[0], getColor(e.path[0]) )
});
