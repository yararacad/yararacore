#!/usr/bin/env python
import sys
import yararacad as yc
import importlib.util
from typing import Callable
from collections.abc import Iterator
from inspect import getfullargspec, FullArgSpec

class Flag:
	def __init__(self, name, char=None, description=""):
		self.name = name
		if char is None and len(name) == 1:
			char = name
		self.char = char
		self.value = False
		self.description = description

	def set(self):
		self.value = True

	def __str__(self):
		return str(self.value)

	def __bool__(self):
		return bool(self.value)

class Parameter(Flag):
	def __init__(self, name, char=None, type=None, value=None, description=""):
		super().__init__(name, char, description)
		self.value = value
		self.type = type

	def convert_type(self, value):
		return (self.type if self.type is not None else lambda v : v)(value)

	def set(self, value):
		self.value = self.convert_type(value)

def handle_short(shortopts, opt: str, args: Iterator):
	try:
		shortopts[opt[0]].set()
		if len(opt) > 1:
			handle_short(shortopts, opt[1:], args)
	except TypeError: #option requires an argument
		if len(opt) > 1:
			shortopts[opt[0]].set(opt[1:])
		else:
			shortopts[opt].set(next(args))

def handle_long(longopts, opt: str, args: Iterator):
	try:
		longopts[opt].set()
	except (TypeError, KeyError): #option requires an argument
		try:
			opt, value = opt.split('=')
		except ValueError: #option uses space instead of '='
			value = next(args)

		longopts[opt].set(value)

def handle_model_args(model_output: Callable, args: Iterator, return_parameters=False):
	parameters = list(get_parameters(model_output))#+ [ Flag("help", 'h') ]
	longopts = { o.name: o for o in parameters }
	shortopts = { o.char: o for o in parameters if o.char }

	for arg in args:
		if arg[0] == '-':
			if arg[1] == '-':
				handle_long(longopts, arg[2:], args)
			else:
				handle_short(shortopts, arg[1:], args)
		else:
			break #FIXME should not consume unhandled arguments

	if return_parameters:
		return parameterise(model_output, *parameters), parameters
	return parameterise(model_output, *parameters)

def main():
	opts = [
			Parameter('export-type', 'x', str),
			Parameter('output', 'o', str),
			Flag('save-params', 's', 'include parameters in the output filename'),
			Flag('print-filename', 'p')
	]
	longopts = { o.name: o for o in opts }
	shortopts = { o.char: o for o in opts if o.char }

	args = iter(sys.argv[1:])
	for arg in args:
		if arg[0] == '-':
			if arg[1] == '-':
				handle_long(longopts, arg[2:], args)
			else:
				handle_short(shortopts, arg[1:], args)
		else:
			#TODO error if this code path is never triggered
			name, output = get_model_output(import_file(arg))

			save_params = longopts['save-params']
			if isinstance(output, Callable):
				output = handle_model_args(output, args, save_params)

			#NOTE output could be either Exportable or (Exportable, list[Parameter]) depending on save_params

			try:
				name = longopts['output'].value or next(args)
			except StopIteration:
				pass

			if save_params:
				output, params = output
				name, extension = name_and_extension(name)
				name = name + '-'.join([ "%s%s" % (p.name, p.value) for p in params ]) + extension

			name += longopts['export-type'].value or ''

			name = yc.export(output, name)
			if longopts['print-filename']:
				print(name)

def parameterise(f: Callable, *parameters):
	return f(**{p.name:p.value for p in parameters})

def valid_model_outputs(model_state: Iterator):
	for (k, v) in model_state:
		if returns_valid_output(v):
			yield(k, v)
			yield from [(k, f) for (k, f) in model_state if returns_valid_output(f)]
		if is_valid_output(v):
			yield (k, v)

def get_model_output(model):
	model_state = iter(model.__dict__.items())
	return list(valid_model_outputs(model_state))[-1]

def get_parameters(f: Callable):
	spec = getfullargspec(f)
	for i in range(len(spec.args)):
		name = spec.args[i]
		try:
			value = spec.defaults[i]
		except IndexError:
			value = None
		try:
			_type = spec.annotations[name]
		except KeyError:
			_type = type(value) if value is not None else None

		yield Parameter(name, type=_type, value=value)

def basename(path: str):
	return path.split('/')[-1]

def name_and_extension(filename: str):
	a = filename.split('.')
	if len(a) > 1:
		return '.'.join(a[:-1]), a[-1]
	return filename, ''

def basename_without_extension(path):
	return name_and_extension(basename(path))[0]

def import_file(path):
	name = basename_without_extension(path)
	spec = importlib.util.spec_from_file_location(name, path)
	module = importlib.util.module_from_spec(spec)
	spec.loader.exec_module(module)
	return module

def return_type(f: Callable):
	try:
		return f.__annotations__["return"]
	except KeyError:
		return None

def is_valid_output(o):
	try:
		return issubclass(o, yc.Exportable)
	except TypeError:
		return False

def returns_valid_output(f):
	return isinstance(f, Callable) and is_valid_output(return_type(f))

if __name__ == "__main__":
	main()
