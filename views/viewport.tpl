<script type="module" src="https://unpkg.com/@google/model-viewer/dist/model-viewer.min.js"></script>
<model-viewer id="viewport" camera-controls src="{{src}}" orientation="0 -90deg 0" camera-orbit="45deg 45deg 45deg" shadow-intensity="1">
	<script src="static/model-viewer-auto-color.js"></script>
</model-viewer>
