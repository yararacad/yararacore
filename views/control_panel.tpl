% rebase('panel.tpl')
% if defined('name') and name is not None:
<h2>{{name}}</h2>
% end
% include('form.tpl', base=form_base)
% include('links.tpl', base=links)
