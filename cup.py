import cadquery as cq
body_radius = 50
body_height = 50
loft_length = 20
foundation_height = 10
foundation_radius = 40
thickness = 7

handle_inner_height = 30
handle_inner_width = 17
handle_width = 15
handle_offset = 0.5
handle_thickness = ht = 10

def max_fillet(*widths):
    return min(*widths) * 0.496

def cup(foundation_height, foundation_radius, body_height, body_radius):
    return cq.Workplane("XY").cylinder(body_height, body_radius).faces(">Z").circle(body_radius)\
        .workplane(offset=loft_length).cylinder(foundation_height, foundation_radius).faces(">Z").circle(foundation_radius)\
        .loft(combine=True)

body = cup(foundation_height, foundation_radius, body_height, body_radius)\
    .hole(2 * body_radius - thickness)

x_radius = handle_inner_height / 2
y_radius = handle_inner_width - (handle_inner_width * handle_offset / 2 )
offset = handle_offset * y_radius

handle_minor_radius = handle_inner_width - offset
handle = cq.Workplane("ZY")\
    .center(0, body_radius + offset)\
    .ellipse(x_radius + ht, y_radius + ht)\
    .extrude(handle_width)\
    .translate((handle_width/2,0,0))\
    .ellipse(x_radius, y_radius)\
    .cutThruAll()

fillet_radius = max_fillet(
    thickness,
    handle_width,
    handle_thickness,
    (body_height - handle_inner_height - 2*ht) / 2
)

result = body.union(handle).cut(cup(foundation_height-thickness, foundation_radius-thickness, body_height-thickness, body_radius-thickness))\
    #.fillet(fillet_radius)