import cadquery as cq
import pint
units = pint.UnitRegistry()

class _testable():
    def __test__(self):
        print(self)
        return True

class length(float, _testable):
    def __new__(self, value):
        try:
            return float.__new__(self, value)
        except ValueError:
            return self(units(str(value)).to(units.millimeter).magnitude)

class unit_interval(float, _testable):
    def __new__(self, value):
        f = lambda v : float.__new__(self, v)
        return max(min(f(value), f(1.0)), f(0.0))

    def __test__(self):
        return super.__test__() and self <= 1 and self >= 0


class Exportable(cq.Workplane):
    def export_stl(self):
        return self.to_compound().exportStl()

def cylinder(height, radius):
    return Exportable().cylinder(height, radius)

def cuboid(x,y,z):
    return Exportable().box(x,y,z)

def export(model, name="untitled.stl", *args, **kwargs):
    try:
        cq.exporters.export(model, name, *args, **kwargs)
        return name
    except ValueError:
        if name.split('.')[-1] == 'json':
            return export(model, name, exportType=cq.exporters.ExportTypes.TJS)
        if name.split('.')[-1] == 'gltf':
            cq.Assembly(model).save(name)
            return name

        name += '.stl'
        return cq.exporters.export(model, name)

def __test__():
    def test(o):
        assert(o.__test__())

    for v in [ 0, 1, 0.5, 0.0000001, 1.1, 300, -0.001, "12", "00.05", "-19.4" ]:
        test(unit_interval(v))

    for v in [ 0, 1, 1200, -8]:
        test(unit_interval(v))

if __name__ == "__main__":
    from sys import argv
    if argv[1].strip() == "test":
        __test__()
    
