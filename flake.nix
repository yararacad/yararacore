{
  description = "Yararacad core library";

  inputs = {
    cadquery.url = "gitlab:yararacad/cq-flake";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, cadquery, flake-utils }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ]( system:
      with import nixpkgs { inherit system; };
      with cadquery.packages.${system}; {
        packages.defaultShell = python.withPackages(ps: with ps; [
          bottle
          pint
        ] ++ (cadquery-libs ps) );
        defaultPackage = pkgs.python3Packages.buildPythonPackage rec {
            name = "yararacool";
            src = ./.;
        };
      }
    );
}
