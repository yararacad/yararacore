#!/usr/bin/env sh

set -e #exit if any error occurs
set -x #show all commands that are run

not() {
    if $@; then
        false
    else
        true
    fi >/dev/null 2>/dev/null
}
testcase_name() {
    echo "testcases/$1.sha256sum"
}

save_testcase() {
    sha256sum "$1" > "$(testcase_name $1)"
}

check() {
    sha256sum --check "$(testcase_name $1)"
}

check_and_rm() {
    check $1 && rm $1
}

yararatool() {
    python -m yararatool --save-params --print-filename $@
}

diff_and_delete() {
    diff $1 $2 && rm $2
}

#open_top_box.py
otb="$( yararatool open_top_box.py )"
check "$otb"
diff_and_delete "$otb" "$( yararatool open_top_box.py -x 3 -y 3 -z 3 --thickness 1 )"
diff_and_delete "$otb" "$( yararatool open_top_box.py -x 3.0 -y 03.000 --z 3 --thickness=01 )"
diff_and_delete "$otb" "$( yararatool open_top_box.py --y=3 )"

big_otb="$( yararatool open_top_box.py -x $((2**17)) -y $((2**23)) -z $((2**16)) --thickness $((2**14)))"
not diff "$otb" "$big_otb"
check_and_rm "$big_otb" && rm "$otb"

not yararatool -o bigger_otb open_top_box.py -x $((2**17)) -y $((2**24)) -z $((2**16)) --thickness $((2**14))

thin_otb="$( yararatool open_top_box.py --thickness 0.001)"
save_testcase "$thin_otb"
diff_and_delete "$thin_otb" "$( yararatool open_top_box.py -x 3 --z=3 --thickness 0.001)"
rm "$thin_otb"

not yararatool open_top_box.py --thickness 0.0001
#not yararatool open_top_box.py -x 0.1

check "$(yararatool mug.py)"

# misc. tests
python test.py
